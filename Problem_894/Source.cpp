#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  unsigned long long N;
  unsigned long long K;

  cin >> N >> K;

  cout << (N - 1 > K ? N - 1 - K : 0) << ' ' << ((N * N - N >> 1) > K ? (N * N - N >> 1) - K : 0);

  return 0;
}